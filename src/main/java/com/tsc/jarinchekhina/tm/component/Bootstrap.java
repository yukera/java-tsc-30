package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.repository.IUserRepository;
import com.tsc.jarinchekhina.tm.api.service.*;
import com.tsc.jarinchekhina.tm.command.AbstractCommand;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.system.UnknownArgumentException;
import com.tsc.jarinchekhina.tm.exception.system.UnknownCommandException;
import com.tsc.jarinchekhina.tm.repository.CommandRepository;
import com.tsc.jarinchekhina.tm.repository.ProjectRepository;
import com.tsc.jarinchekhina.tm.repository.TaskRepository;
import com.tsc.jarinchekhina.tm.repository.UserRepository;
import com.tsc.jarinchekhina.tm.service.*;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.SystemUtil;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Getter
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final ILogService logService = new LogService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        initCommands(commandService.getCommandList());
        initUsers();
        initData();
        initPID();
    }

    private void initCommands(@NotNull final List<AbstractCommand> commandList) {
        for (@NotNull AbstractCommand command : commandList) initCommand(command);
    }

    private void initCommand(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initUsers() {
        userService.create("test", "test", "test@test.ru");
        userService.create("testdouble", "testdouble", "testdouble@test.ru");
        userService.create("admin", "admin", Role.ADMIN);
    }

    private void initData() {
        authService.login("test", "test");
        @NotNull String userId = authService.getUserId();
        projectService.create(userId, "Project 1").setStatus(Status.IN_PROGRESS);
        projectService.create(userId, "PROJECT").setStatus(Status.COMPLETED);
        taskService.create(userId, "TASK").setStatus(Status.IN_PROGRESS);
        authService.logout();

        authService.login("testdouble", "testdouble");
        userId = authService.getUserId();
        projectService.create(userId, "ASDSAD1");
        taskService.create(userId, "Task 1");
        taskService.create(userId, "adskJN").setStatus(Status.COMPLETED);
        authService.logout();
    }

    @SneakyThrows
    private void parseArgs(@Nullable final String[] args) {
        if (DataUtil.isEmpty(args)) return;
        final String arg = args[0];
        parseArg(arg);
        System.exit(0);
    }

    @SneakyThrows
    private void parseArg(@Nullable final String arg) {
        if (DataUtil.isEmpty(arg)) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) throw new UnknownArgumentException(arg);
        command.execute();
    }

    @SneakyThrows
    public void parseCommand(@NotNull final String cmd) {
        if (DataUtil.isEmpty(cmd)) throw new UnknownCommandException(cmd);
        @Nullable final AbstractCommand command = commandService.getCommandByName(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    @SneakyThrows
    public void start(@Nullable final String[] args) {
        logService.info("*** Welcome to Task Manager ***");
        parseArgs(args);
        backup.init();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                System.out.println("[OK]");
            } catch (Exception e) {
                logService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    @SneakyThrows
    public void initPID() {
        @NotNull final String fileName = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        final File file = new File(fileName);
        file.deleteOnExit();
    }

}