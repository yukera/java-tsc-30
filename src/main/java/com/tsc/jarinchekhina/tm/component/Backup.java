package com.tsc.jarinchekhina.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup {

    @NotNull
    private static final String CMD_SAVE = "backup-save";

    @NotNull
    private static final String CMD_LOAD = "backup-load";

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @SneakyThrows
    public void init() {
        load();
        start();
    }

    @SneakyThrows
    public void start() {
        @NotNull final Long delay = bootstrap.getPropertyService().getBackupInterval();
        executorService.scheduleWithFixedDelay(this::save,0, delay, TimeUnit.SECONDS);
    }

    public void stop() {
        executorService.shutdown();
    }

    @SneakyThrows
    public void save() {
        bootstrap.parseCommand(CMD_SAVE);
    }

    @SneakyThrows
    public void load() {
        bootstrap.parseCommand(CMD_LOAD);
    }

}
