package com.tsc.jarinchekhina.tm.entity;

import com.tsc.jarinchekhina.tm.api.entity.IWBS;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
public final class Project extends AbstractEntity implements IWBS {

    @NotNull
    private String userId;

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @Nullable
    private Date created = new Date();

    @Override
    public String toString() {
        return getId() + ": " + getName();
    }

}
