package com.tsc.jarinchekhina.tm.enumerated;

import com.tsc.jarinchekhina.tm.comparator.ComparatorByCreated;
import com.tsc.jarinchekhina.tm.comparator.ComparatorByDateStart;
import com.tsc.jarinchekhina.tm.comparator.ComparatorByName;
import com.tsc.jarinchekhina.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    CREATED("sort by created", ComparatorByCreated.getInstance()),
    DATE_START("sort by date start", ComparatorByDateStart.getInstance()),
    NAME("sort by name", ComparatorByName.getInstance()),
    STATUS("sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
