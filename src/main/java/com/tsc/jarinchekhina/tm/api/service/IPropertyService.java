package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.other.ISaltSetting;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getValue(@Nullable String key, @Nullable String defaultValue);

    @NotNull
    String getVersion();

    @NotNull
    String getDeveloper();

    @NotNull
    String getDeveloperEmail();

    @NotNull
    Long getBackupInterval();

}
